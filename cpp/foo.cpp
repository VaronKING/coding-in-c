#include <iostream>

/* Same rules as C apply here, except we can use namespace line to 
 * allow us to use cout without having to type std::cout.
 * Think of it like importing a library (or one of its elements). */


using namespace std;

int main() {
	int a, b; string X;

	//This is how you use cin to insert values.
	cin >> a >> b >> X;

	/* This is how you use cout on multiple lines,
	 * and how you insert multiple values into one cout.
	 * You can use endl instead of << "\n" for inserting new
	 * lines. */
	cout << "Hello world!\n"
	     << "This is the value of a and b: " << a << ", " << b << endl
	     << "And this is the value of a+b: " << a + b << "; and the string X: " << X << endl;

	// If statements are the same as in C.
	if (a < b) X = "a is less than b.";
	else if (a > b) X = "a is greater than b";
	else X = "a is equal to b.";

	cout << X << endl;

	// Now we do some math!
	int i; int c = a;
	for (i = 0; i < 3; i++) {
		a+=b;
		b+=5;
		c=a*b;
		cout << "This is loop number: " << i+1 << endl
		<< "\ta, b and c are equal to: "
		<< a << ", " << b << ", " << c << endl;
	}

	return 0;
}
