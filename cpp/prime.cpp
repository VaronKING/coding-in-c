#include <iostream>
#define n 10

using namespace std;

bool checkifPrime(int num);

int main() {
  int x, lim;
  cout << "Enter your limit: "; cin >> lim;
  
  for (unsigned int i = 1; i <= lim; i++)
    if (checkifPrime(i)) cout << i << endl;

  return 0;
}

bool checkifPrime(int num) {

  int cntr = 0;
  for (unsigned int i = 1; i <= num; i++) {
     if (!(num % i)) cntr++;
  }
  
  if (cntr == 2) return 1;
  else return 0;

}
