#include <iostream>
//#include <ctime>
#include <random>

using namespace std;

int main () {

  /* We can instead use the rand() function like this: rand() %(high - low + 1) + low
     However, we'd need to use srand(time(NULL)) beforehand to generate a random seed.
     Generally, it is recommended to use the below methods, relying on the random library. */
  
  int choice;

  do {

  random_device rd; // non-deterministic engine (RESEARCH THIS)
  mt19937 eng(rd()); // seed the engine with a random value (AND THIS)
  uniform_int_distribution<int> dist(-5, 5); // a value between 1 and 50 (AND THIS!)
  int rng = dist(eng);

  //srand(time(0)); int rng = rand() %(50 - 0 + 1) //(EXPERIMENT WITH THIS);

  cout << rng << endl;

  cout << "Try again? (0/1) ";
  cin >> choice;

  } while(choice != 0);
  
return 0;
}
