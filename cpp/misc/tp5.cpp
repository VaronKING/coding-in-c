#include <iostream>
// x ^ y

using namespace std;

int power(int x, int y);

int main() {
  int x, y;
  cin >> x >> y;
  cout << "x^y=" << power(x, y) << endl;
  return 0;
  }

/*int power(int x, int y) {
  int result;
  if (y == 0) result = 1;
  else {
    result = x;
    for (int i = 1; i < y; i++) {
      result *= x;
    }
  }
  return result;
}*/

int power(int x, int y) {
  int result;
  if (y == 0) result = 1;
  else {
    result = x; int temp = x;
    for (int i = 1; i < y; i++) {
      for (int j = 1; j < x; j++) result += temp;
      temp = result;
    }
  }
  return result;
}

