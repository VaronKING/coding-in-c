#include <iostream>
#define n 10

using namespace std;

int main() {
  
  int list[n], i;

  for (i=0; i < 10; i++) {
    cout << "Enter the " << i+1 << "th element: ";
    cin >> list[i];
    }
  
  int max = list[0], min = list[0]; float avg = 0;
  for (i=0; i < 10; i++) {
    if (max < list[i]) max = list[i];
    if (min > list[i]) min = list[i];
    avg += list[i];
    }
  
  // Array average, and length
  avg /= 10.0;
  int sl = sizeof(list) / sizeof(list[0]);
  
  cout << "Results:\t" << max << ",\t" << min << ",\t" << avg << ",\t" << sl << endl;

  return 0;
}
