#include <iostream>
#include <random>

using namespace std;

int main () {

// Exercise 1
cout << "Hello world!" << endl;

// Exercise 2
string userName; int userAge;
cout << "Please enter your name and age (in that order): "; cin >> userName >> userAge;
cout << "You are " << userName << " and you are " << userAge << " years old." << endl;

// Exercise 3
int a, b;
cout << "Please enter two numbers: "; cin >> a >> b;
cout << "Their sum is: " << a+b << endl;

// Exercise 4
int A, B, C;
cout << "Please enter two numbers (A and B): "; cin >> A >> B;
C = A, A = B, B = C;
cout << "Here are the new A and B: " << A << ", " << B << endl;
 
// Exercise 5
int testnum;
cout << "Enter a number: "; cin >> testnum;
if (testnum % 2 == 0) cout << testnum << " is even." << endl;
else cout << testnum << " is odd." << endl;
 
// Exercise 6
int grade;
do {
  cout << "Please enter your grade: "; cin >> grade;

  if (grade < 0 || grade > 100) cout << "Please enter a grade between 0-100." << endl;
  else if (grade >= 90) cout << "You got an A." << endl;
  else if (grade >= 80) cout << "You got a B." << endl;
  else if (grade >= 70) cout << "You got a C." << endl;
  else if (grade >= 60) cout << "You got a D." << endl;
  else cout << "You got an F." << endl;

} while (grade < 0 || grade > 100);

// Exercise 7
int num, i;
cout << "Please enter a number: "; cin >> num;
cout << "Here is its multiplication table: " << endl;
for (i = 0; i <= 10; i++) cout << num * i << endl;
 
//Exercise 8
int fact, result, j;

do {

cout << "Please enter your number: "; cin >> fact;

j = 1, result = fact;

if (fact < 0 ) cout << "Enter a positive number." << endl;
else if (fact == 0 || fact == 1) result = 1;
else while (j < fact-1) {

result = result * (fact - j);
j++;

}
 

} while (fact < 0);

cout << "Your factorial is: " << result << endl;

// Exercise 9 
/* We can use the rand() function like this: rand() %(high - low + 1).
   However, we'd need to use srand(time(0)) beforehand to generate a random seed.
   srand(time(0) generates a seed based on the current system time. */

srand(time(0));
int rng = rand() %50, guess;

do {

  do {

  cout << "Please guess the number I have generated." << endl; cin >> guess;
  if (guess < 1 || guess > 50) cout << "The number generated is between 1 & 50." << endl;
  else if ((rng - guess) < 0) cout << "Your guess is too high!" << endl;
  else if ((rng - guess) > 0 && rng != guess) cout << "Your guess is too low!" << endl;
  else cout << "Your guess is correct!" << endl;

  } while (guess != rng);

} while (guess < 1 || guess > 50);

return 0;
}

