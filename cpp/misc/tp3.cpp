#include <iostream>

using namespace std;

int main() {
  
  float table[3][3], avg = 0;
  int i, j;

  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      cout << "Enter a number: "; cin  >> table[i][j];
      }
    }
  
  cout << endl << "Here is your 3x3 table: " << endl;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      cout << table[i][j] << " \t";
      avg += table[i][j];
      }
    cout << endl;
    }

  cout << endl << "The total average is: " << avg/9.0 << endl;
					
  avg = 0;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      avg += table[i][j];
      }
    cout << "The line average is: (" << i+1 << "): " << avg/3.0 << endl;
    avg = 0;
    }

  for (j = 0; j < 3; j++) {
    for (i = 0; i < 3; i++) {
      avg += table[i][j];
      }
    cout << "The column average is: (" << j+1 << "): " << avg/3.0 << endl;
    avg = 0;
    }

  return 0;
  }
