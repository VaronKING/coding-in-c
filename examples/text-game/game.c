#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[20];
    char desc[50];
    int hp;
} enemy;

void say(char *word);
void hit(enemy *mob);
void examine(enemy *mob);
void action();

int main() {
    enemy Goblin;
    Goblin.hp = 3;
    strcpy(Goblin.name, "Goblin");
    strcpy(Goblin.desc, "A foul creature.");
    printf("You are strolling through the forest, when you suddenly encounter a(n) %s!\n", Goblin.name);

    do {
        action();
        int choice;
        scanf("%i", &choice);

        char word[10];
        switch(choice) {
            case 1:
                say(word);
                break;
            case 2:
                hit(&Goblin);
                break;
            case 3:
                examine(&Goblin);
                break;
            case 4:
                printf("\nYou ran away. Coward.\n");
                exit(0);
                break;
            default:
                printf("\nTry again.\n");
        }

        if (Goblin.hp == 0) {
            printf("You win!\n");
            exit(0);
        }

    } while(Goblin.hp != 0); // Constantly request an action until the enemy is dead


    return 0;
}

void say(char *word) {
    scanf("%s", word);

    if (strcmp(word, "Linux") == 0 || strcmp(word, "linux") == 0) printf("\nIt's `Gnu/Linux`!\n");
    else printf("\nYou said `%s`!\n", word);
}

void hit(enemy *mob) {
    switch(mob->hp) {
        case 3:
            printf("\nIt has been injured.\n");
            mob->hp--;
            break;

        case 2:
            printf("\nIt is bleeding profusely!\n");
            mob->hp--;
            break;

        case 1:
            printf("\nIt is dead.\n");
            mob->hp--;
            break;

        case 0:
            break;

        default:
            printf("\n%s\n", mob->desc);
            mob->hp--;
            break;
    }

}

void examine(enemy *mob) {
    switch(mob->hp) {
        case 3:
            printf("\n%s\n"
                   "Maybe try hitting it?\n"
                   , mob->desc);
            break;

        case 2:
            printf("\nIt is wounded.\n");
            break;

        case 1:
            printf("\nIt is close to death!\n");
            break;

        case 0:
            printf("\nIt's still dead.\n");
            break;

        default:
            printf("\n%s\n", mob->desc);
            break;
    }
}

void action() {
    printf("\nWhat will you do? (1|2|3)\n"
           "1. Say something.\n"
           "2. Hit it!\n"
           "3. Examine it.\n"
           "4. Run away.\n");
}
