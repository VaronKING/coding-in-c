#include <stdio.h>

int main() {
    unsigned long long A = 0, B = 1, C;
    int n, i;

    printf("How many numbers do you want in the sequence? ");
    scanf("%d", &n);

    printf("[%lld, %lld, ", A, B);
    for(i = 0; i < n; i++) {
        C = A + B;

        if(i < n-1) printf("%lld, ", C);
        else printf("%lld]\n", C);

        A = B;
        B = C;
    }

    return 0;
}
