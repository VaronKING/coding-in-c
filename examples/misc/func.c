#include <stdio.h>

// Defining a void function. Note how it does not return a value to be used later.
void read(int *a) {
    printf("Please enter a positive number: ");
    scanf("%i", a);
}

void inverse(int *p, int *g) {
    if (*p > *g) {
        int q = *g; *g = *p; *p = q;
    }
}

// An integer function. Note the "return (n % d == 0)" line, which returns a 1 or 0 value (true or false).
int division(int n, int d) {
    // Instead of using if statements, we return the result of the condition directly.
    return (n % d == 0);
}

int perfect(int x) {
    int i, tot = 1;
    for (i = 2; i <= x/2; i++) {
        // Instead of doing a comparison, this simply checks if the returned value is true and proceeds onwards if it is.
        if (division(x, i)) tot = tot + i;
    }
    return (x == tot);
}

int main() {
    int x,y,i;
    read(&x), read(&y);
    inverse(&x, &y);

    for (i = x; i <= y; i++){
        // perfect is called on i, which returns 1 or 0.
        // Instead of redirecting output to an extra variable, we return the output of the function directly,
        // because it is not void.
        if (perfect(i)) printf("\t%i\n", i);
    }

    return 0;
}
