#include <stdio.h>
#include <stdlib.h> // For malloc
#include <time.h> // For struct tm, used for time formatting

// Constant n is the number of employees (structure variables)
#define n 3

/* We use typedef struct to give a type to the structure, which is how we refer to it, instead of typing struct name everytime
 * The name of the structure in this case is optional, since we use the type name */
typedef struct employeeFile {
    int ID;
    char Name[50];
    char Sex;
    struct tm Date; // A nested structure called Date
    float Salary;
} employee;

// All these functions take a structure pointer as a parameter. Used later in the for loops
void insertDatabase(employee *person);
void printDatabase(employee *person, int *checkID);
void compareDate(employee *person, int *checkYear);
void compareSalary(employee *person, float *checkSalary);

int main() {
    employee *database;
    int pass, year;
    float payment;

    /* Memory allocation (malloc) is used here. We allocate 120 * n bytes of memory for the employee structure.
     * The syntax for malloc is: (castType*) malloc(size); */
    database = (employee*) malloc(sizeof(employee) * n);

    insertDatabase(database);
    printDatabase(database, &pass);
    compareDate(database, &year);
    compareSalary(database, &payment);

    free(database);

    return 0;
}

/* In each of the following functions, we use a for loop with (ptr+i) in place of ptr whenever we mess with I/O. This is so we can create n structure variables,
 * which we access using a for loop with the integer i.
 * Think of this like an array, but we use memory allocation instead.
 * As far as I know, this is required if you want to use for loops in the functions themselves, instead of the main function. */

void insertDatabase(employee *person) {
    int i;
    for (i = 0; i < n; i++) {
        printf("You are on employee number %i. \n", i+1);
        (person+i)->ID = i+1;

        printf("Enter a name (<= 50 characters): ");
        scanf("%s", (person+i)->Name);

        printf("Enter a sex (mM/fF): ");
        scanf("%s", &(person+i)->Sex);

        printf("Enter a date (DD/MM/YYYY):\n");
        scanf("%i %i %i", &(person+i)->Date.tm_mday, &(person+i)->Date.tm_mon, &(person+i)->Date.tm_year);

        printf("Enter a salary ($): ");
        scanf("%g", &(person+i)->Salary);

        printf("\n");
    }
}

void printDatabase(employee *person, int *checkID) {
    int i;
    printf("Enter an ID: ");
    scanf("%i", checkID);

    for (i = 0; i < n; i++)
        if ((person+i)->ID == *checkID)
            printf("\nHere is your information:\n"
                   "\tID:\t%i\n"
                   "\tName:\t%s\n"
                   "\tSex:\t%c\n"
                   "\tRecruitment date:\t%i/%i/%i\n"
                   "\tSalary:\t$%g\n"

                   , (person+i)->ID
                   , (person+i)->Name
                   , (person+i)->Sex
                   , (person+i)->Date.tm_mday
                   , (person+i)->Date.tm_mon
                   , (person+i)->Date.tm_year
                   , (person+i)->Salary);
}

void compareDate(employee *person, int *checkYear) {
    int i, counter = 0;
    printf("\nEnter a year: ");
    scanf("%i", checkYear);

    for (i = 0; i < n; i++) if ((person+i)->Date.tm_year < *checkYear) counter++;
    printf("There are %i employee(s) recruited before the year %i.\n", counter, *checkYear);
}

void compareSalary(employee *person, float *checkSalary) {
    int i;
    printf("\nEnter a salary: ");
    scanf("%g", checkSalary);

    for (i = 0; i < n; i++)
        if ((person+i)->Salary < *checkSalary)
            printf("Employee %s (ID %i) has a salary less than $%g. ($%g)\n"

                   , (person+i)->Name, (person+i)->ID
                   , *checkSalary
                   , (person+i)->Salary);
}
