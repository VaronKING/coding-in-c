#include <stdio.h>
#include <math.h>

void insert(int *x) {
    // A do while loop is used to constantly ask for an integer if
    // it does not meet the listed conditions.
    do {
        printf("Please insert an integer between 100 and 999: ");
        scanf("%i", x);
        if (*x < 100) printf("Please insert a bigger integer.\n");
        else if (*x > 999) printf("Please insert a smaller integer.\n");
    } while (*x < 100 || *x > 999);
}

int cube(int n) {
    // pow is a built in function, part of the math.h library.
    // Add the -lm flag for `link to libm.h` when running gcc.
    return (n == pow((n / 100),3) + pow(((n / 10) % 10),3) + pow((n % 10),3));
}

int main() {
    int x;

    insert(&x);

    if (cube(x)) printf("%i is a cubic number.\n", x);
    else printf("%i is not a cubic number.\n", x);

    return 0;
}
