#include <stdio.h>
#include <time.h>

#define n 3

// This code is not commented and may be inoptimized. Check struct-malloc.c for a better example.

typedef struct employeeFile {
    int ID;
    char Name[50];
    char Sex;
    struct tm Date;
    float Salary;
} employee;

void insertData(employee *person);
void printData(employee person);
int compareDate(employee person, int year);
int compareSalary(employee person, float checkSalary);

int main() {
    employee database[n];
    int i;
    for (i = 0; i < n; i++) {
        database[i].ID = i+1;
        insertData(&database[i]);
    }

    int checkID; int year = 2000; float salary = 100; int counter = 0;
    printf("Enter your ID: ");
    scanf("%i", &checkID);
    for (int i = 0; i < n; i++) {
        if (checkID == database[i].ID)
            printData(database[i]);

        if (compareDate(database[i], year)) counter++;
        if (compareSalary(database[i], salary))
        printf("%s (ID: %i) has a lower salary than $%g ($%g).\n"
               , database[i].Name
               , database[i].ID
               , salary
               , database[i].Salary);
    }
    printf("\nThe number of people hired prior to the year %i is: %i.\n", year, counter);

    return 0;
}

void insertData(employee *person) {
    printf("Enter a name (<= 50 characters): ");
    scanf("%s", person->Name);

    printf("Enter a sex (mM/fF): ");
    scanf("%s", &person->Sex);

    printf("Enter a date (DD/MM/YYYY):\n");
    scanf("%i %i %i", &person->Date.tm_mday, &person->Date.tm_mon, &person->Date.tm_year);

    printf("Enter a salary: ");
    scanf("%g", &person->Salary);

    printf("\n");
}

void printData(employee person) {
    printf("\nHere is your information:\n"
           "\tID:\t%i\n"
           "\tName:\t%s\n"
           "\tSex:\t%c\n"
           "\tRecruitment date:\t%i/%i/%i\n"
           "\tSalary:\t$%g\n"

           , person.ID
           , person.Name
           , person.Sex, person.Date.tm_mday
           , person.Date.tm_mon
           , person.Date.tm_year
           , person.Salary);
}

int compareDate(employee person, int year) {
    return (person.Date.tm_year < year);
}

int compareSalary(employee person, float checkSalary) {
    return (person.Salary < checkSalary);
}
